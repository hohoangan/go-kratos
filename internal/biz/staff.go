package biz

import (
	"context"

	"github.com/go-kratos/kratos/v2/log"
)

// NewNhanVienUsecase is a NhanVien usecase.
type NhanVienUsecase struct {
	repo NhanVienRepo
	log  *log.Helper
}

// NewNhanVienUsecase new a NhanVien usecase.
func NewNhanVienUsecase(repo NhanVienRepo, logger log.Logger) *NhanVienUsecase {
	return &NhanVienUsecase{repo: repo, log: log.NewHelper(logger)}
}

// NhanVien is a NhanVien model.
type NhanVien struct {
	MaNV  int    `gorm:"column:MaNV;type:int;primary_key"`
	TenNV string `gorm:"column:TenNV"`
}

// Trong trường hợp không muốn sử dụng tên mặc định của bảng NhanVien, bạn có thể chỉ định tên bảng là "NhanVien"
func (NhanVien) TableName() string {
	return "NhanVien"
}

// NhanVien is a Greater repo.
type NhanVienRepo interface {
	Save(context.Context, *NhanVien) (*NhanVien, error)
	Update(context.Context, *NhanVien) (*NhanVien, error)
	Delete(context.Context, int64) (*NhanVien, error)
	FindByID(context.Context, int64) (*NhanVien, error)
	ListByHello(context.Context, string) ([]*NhanVien, error)
	ListAll(context.Context) ([]*NhanVien, error)
}

func (uc *NhanVienUsecase) CreateStaff(ctx context.Context, g *NhanVien) (*NhanVien, error) {
	uc.log.WithContext(ctx).Infof("CreateStaff: %v", g.TenNV)
	return uc.repo.Save(ctx, g)
}

func (uc *NhanVienUsecase) UpdateStaff(ctx context.Context, g *NhanVien) (*NhanVien, error) {
	uc.log.WithContext(ctx).Infof("UpdateStaff: %v", g)
	return uc.repo.Update(ctx, g)
}
func (uc *NhanVienUsecase) DeleteStaff(ctx context.Context, maNV int) (*NhanVien, error) {
	uc.log.WithContext(ctx).Infof("DeleteStaff: %v", maNV)
	return uc.repo.Delete(ctx, int64(maNV))
}
func (uc *NhanVienUsecase) FindByIDStaff(ctx context.Context, maNV int) (*NhanVien, error) {
	uc.log.WithContext(ctx).Infof("FindByID - MaNV: %v", maNV)
	return uc.repo.FindByID(ctx, int64(maNV))
}
func (uc *NhanVienUsecase) ListAllStaff(ctx context.Context) ([]*NhanVien, error) {
	uc.log.WithContext(ctx).Infof("ListAllStaff")
	return uc.repo.ListAll(ctx)
}
