package data

import (
	"context"

	"kratos-fw/internal/biz"

	"github.com/go-kratos/kratos/v2/log"
)

type nhanVienRepo struct {
	data *Data
	log  *log.Helper
}

// NewNhanVienRepo .
func NewNhanVienRepo(data *Data, logger log.Logger) biz.NhanVienRepo {
	return &nhanVienRepo{
		data: data,
		log:  log.NewHelper(logger),
	}
}

func (r *nhanVienRepo) Save(ctx context.Context, g *biz.NhanVien) (*biz.NhanVien, error) {
	result := r.data.db.Create(g)
	if result.Error != nil {
		r.log.Errorf("Failed to insert NhanVien: %v", result.Error)
		return nil, result.Error
	}
	return g, nil
}

func (r *nhanVienRepo) Update(ctx context.Context, g *biz.NhanVien) (*biz.NhanVien, error) {
	err := r.data.db.Model(&biz.NhanVien{}).Where("MaNV = ?", g.MaNV).Updates(g).Error
	if err != nil {
		r.log.Errorf("Failed to update NhanVien: %v", err)
		return nil, err
	}
	return g, nil
}

func (r *nhanVienRepo) Delete(ctx context.Context, maNV int64) (*biz.NhanVien, error) {
	var nhanVien *biz.NhanVien
	result := r.data.db.Delete(&nhanVien, maNV)
	if result == nil {
		r.log.Errorf("Failed to delete NhanViens: %v", result.Error)
		return nil, result.Error
	}
	return nhanVien, nil
}

func (r *nhanVienRepo) FindByID(ctx context.Context, maNV int64) (*biz.NhanVien, error) {
	var nhanVien *biz.NhanVien
	result := r.data.db.Find(&nhanVien, maNV)
	if result == nil {
		r.log.Errorf("Failed to find NhanViens: %v", result.Error)
		return nil, result.Error
	}
	return nhanVien, nil
}

func (r *nhanVienRepo) ListByHello(context.Context, string) ([]*biz.NhanVien, error) {
	return nil, nil
}

func (r *nhanVienRepo) ListAll(ctx context.Context) ([]*biz.NhanVien, error) {
	var nhanViens []*biz.NhanVien
	result := r.data.db.Find(&nhanViens)
	if result.Error != nil {
		r.log.Errorf("Failed to find NhanViens: %v", result.Error)
		return nil, result.Error
	}
	return nhanViens, nil
}
