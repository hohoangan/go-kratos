package server

import (
	v1 "kratos-fw/api/helloworld/v1"
	nv "kratos-fw/api/staff/v1"
	"kratos-fw/internal/conf"
	"kratos-fw/internal/service"

	"github.com/go-kratos/kratos/v2/log"
	"github.com/go-kratos/kratos/v2/middleware/recovery"
	"github.com/go-kratos/kratos/v2/transport/http"
	"github.com/go-kratos/swagger-api/openapiv2"
)

// NewHTTPServer new an HTTP server.
func NewHTTPServer(c *conf.Server, greeter *service.GreeterService, nhanvien *service.StaffService, logger log.Logger) *http.Server {
	var opts = []http.ServerOption{
		http.Middleware(
			recovery.Recovery(),
		),
	}
	if c.Http.Network != "" {
		opts = append(opts, http.Network(c.Http.Network))
	}
	if c.Http.Addr != "" {
		opts = append(opts, http.Address(c.Http.Addr))
	}
	if c.Http.Timeout != nil {
		opts = append(opts, http.Timeout(c.Http.Timeout.AsDuration()))
	}
	srv := http.NewServer(opts...)
	//Swagger-api
	h := openapiv2.NewHandler()
	srv.HandlePrefix("/q/", h)
	v1.RegisterGreeterHTTPServer(srv, greeter)
	nv.RegisterStaffHTTPServer(srv, nhanvien)
	return srv
}
