package service

import (
	"context"
	"encoding/json"

	pb "kratos-fw/api/staff/v1"
	"kratos-fw/internal/biz"
)

type StaffService struct {
	pb.UnimplementedStaffServer
	uc *biz.NhanVienUsecase
}

func NewStaffService(uc *biz.NhanVienUsecase) *StaffService {
	return &StaffService{uc: uc}
}

func (s *StaffService) CreateStaff(ctx context.Context, req *pb.CreateStaffRequest) (*pb.CreateStaffReply, error) {
	g, err := s.uc.CreateStaff(ctx, &biz.NhanVien{TenNV: req.TenNV})
	if err != nil {
		return nil, err
	}
	return &pb.CreateStaffReply{Message: "Create successfull: " + g.TenNV}, nil
}
func (s *StaffService) UpdateStaff(ctx context.Context, req *pb.UpdateStaffRequest) (*pb.UpdateStaffReply, error) {
	var updateNhanVien = &biz.NhanVien{
		MaNV:  int(req.MaNV),
		TenNV: req.TenNV,
	}
	g, err := s.uc.UpdateStaff(ctx, updateNhanVien)
	if err != nil {
		return nil, err
	}
	return &pb.UpdateStaffReply{Message: "Update successfull: " + g.TenNV}, nil
}
func (s *StaffService) DeleteStaff(ctx context.Context, req *pb.DeleteStaffRequest) (*pb.DeleteStaffReply, error) {
	g, err := s.uc.DeleteStaff(ctx, int(req.MaNV))
	if err != nil {
		return nil, err
	}
	return &pb.DeleteStaffReply{Message: "Delete successfull: " + g.TenNV}, nil
}
func (s *StaffService) GetStaff(ctx context.Context, req *pb.GetStaffRequest) (*pb.GetStaffReply, error) {
	g, err := s.uc.FindByIDStaff(ctx, int(req.MaNV))
	if err != nil {
		return nil, err
	}
	return &pb.GetStaffReply{Message: "Find successfull: " + g.TenNV}, nil
}
func (s *StaffService) ListStaff(ctx context.Context, req *pb.ListStaffRequest) (*pb.ListStaffReply, error) {
	g, err := s.uc.ListAllStaff(ctx)
	if err != nil {
		return nil, err
	}
	var jsonData []byte
	if len(g) > 0 {
		jsonData, err = json.Marshal(g)
		if err != nil {
			return nil, err
		}
	} else {
		// Trả về danh sách rỗng nếu không có nhân viên
		jsonData = []byte("[]")
	}
	return &pb.ListStaffReply{Message: string(jsonData)}, nil
}
